import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Locale;

public class NavigationPanel extends JPanel {

    NavigationPanel(ArrayList<String> countryNames, ArrayList<Locale> countryLocales, ImagePanel iPanel) {
        setLayout(new MigLayout("","[grow,fill]","[grow,fill][fill]"));
        JList countryList = new JList(countryNames.toArray());
        countryList.setDragEnabled(true);
        add(new JScrollPane(countryList), "wrap");
        BorderPanel bPanel = new BorderPanel(countryNames, countryLocales, iPanel);
        add(bPanel);
    }
}
