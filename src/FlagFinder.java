import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Locale;

public class FlagFinder extends JFrame {
    private ImageIcon[] images;
    private int biggestHeight = 0;
    private int biggestWidth = 0;

    FlagFinder(ImageIcon[] imgs) {
        images = imgs;
        ArrayList<String> countryNames = new ArrayList<>();
        ArrayList<Locale> countryLocales = new ArrayList<>();
        for(ImageIcon ii : images) {
            Locale.Builder a = new Locale.Builder();
            Locale l = a.setRegion(ii.getDescription()).build();
            countryNames.add(l.getDisplayCountry());
            countryLocales.add(l);
            if(biggestHeight < ii.getIconHeight()) biggestHeight = ii.getIconHeight();
            if(biggestWidth < ii.getIconWidth()) biggestWidth = ii.getIconWidth();
        }

        setLayout(new MigLayout("","[][grow,fill]","[grow,fill]"));

        ImagePanel iPanel = new ImagePanel(biggestWidth + 2, biggestHeight + 2);
        NavigationPanel nPanel = new NavigationPanel(countryNames, countryLocales, iPanel);
        add(nPanel);
        add(iPanel);
    }
}
