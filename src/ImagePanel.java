import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import java.awt.*;

public class ImagePanel extends JPanel {
    JLabel image = new JLabel("", SwingConstants.CENTER);

    ImagePanel(int width, int height) {
        super();
        setLayout(new MigLayout("","[grow,fill][][grow,fill]","[grow,fill][][grow,fill]"));
        setBackground(Color.GRAY);
        setMinimumSize(new Dimension(width, height));
        add(image, "cell 1 1");
    }

    void setImage(ImageIcon i) {
        image.setIcon(i);
        image.repaint();
    }
}
