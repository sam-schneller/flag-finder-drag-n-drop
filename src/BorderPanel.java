import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.ArrayList;
import java.util.Locale;

public class BorderPanel extends JPanel {
    JTextField entryField = new JTextField();

    BorderPanel(ArrayList<String> countryNames, ArrayList<Locale> countryLocales, ImagePanel iPanel) {
        setLayout(new MigLayout("","[grow,fill]","[grow,fill]"));
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Country Name"), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        entryField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                warn();
            }
            public void removeUpdate(DocumentEvent e) {
                warn();
            }
            public void insertUpdate(DocumentEvent e) {
                warn();
            }

            public void warn() {
                String imageLoc;
                if(countryNames.indexOf(entryField.getText()) > -1) {
                    imageLoc = "src\\Flags\\" + countryLocales.get(countryNames.indexOf(entryField.getText())).getCountry().toLowerCase() + ".png";
                    ImageIcon tempIcon = new ImageIcon(imageLoc);
                    iPanel.setImage(tempIcon);
                }
            }
        });
        add(entryField);
    }
}
