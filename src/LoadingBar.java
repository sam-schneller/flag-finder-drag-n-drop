import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.concurrent.ExecutionException;

public class LoadingBar extends JWindow {
    private int completion = 0;
    private LoadingBar hold;
    private JProgressBar jpb;
    private final int NUMBER_OF_IMAGES = 196;

    private LoadingBar() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        setLocationRelativeTo(null);

        JLabel percentage = new JLabel("Loading Images...", SwingConstants.CENTER);
        c.gridx = 0;
        c.gridy = 0;
        add(percentage, c);

        jpb = new JProgressBar(SwingConstants.HORIZONTAL, 0, 100);
        jpb.setValue(completion);
        jpb.setString(completion + "%");
        jpb.setStringPainted(true);
        c.gridx = 0;
        c.gridy = 1;
        add(jpb, c);

        hold = this;
        loadImages();
    }

    private ImageIcon loadImage(int imageNum) {
        File[] imageFiles = new File("src\\Flags").listFiles();
        if (imageFiles != null) {
            return new ImageIcon(imageFiles[imageNum].toString(), imageFiles[imageNum].getName().substring(0, 2).toUpperCase());
        }
        return null;
    }

    private SwingWorker worker = new SwingWorker<ImageIcon[], Void>() {
        @Override
        public ImageIcon[] doInBackground() {
            final ImageIcon[] innerImgs = new ImageIcon[NUMBER_OF_IMAGES];
            for (int i = 0; i < NUMBER_OF_IMAGES; i++) {
                innerImgs[i] = loadImage(i);
                incrementValue(((i+1) * 100) / NUMBER_OF_IMAGES);
            }
            return innerImgs;
        }

        @Override
        public void done() {
            hold.setVisible(false);

            FlagFinder ff = null;
            try {
                ff = new FlagFinder(this.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            if(ff != null) {
                ff.setTitle("Flag Finder");
                ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                ff.pack();
                ff.setMinimumSize(ff.getPreferredSize());
                ff.setVisible(true);
            }
        }
    };

    private void loadImages() {
        worker.execute();
        this.setVisible(false);
    }

    private void incrementValue(int i) {
        completion = i;
        jpb.setValue(completion);
        jpb.setString(completion + "%");
        this.repaint();
    }

    private static void createGui() {
        LoadingBar lb = new LoadingBar();
        lb.setSize(200, 100);
        lb.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createGui();
            }
        });
    }
}